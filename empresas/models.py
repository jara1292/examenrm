from django.db import models

# Create your models here.
class Empresa(models.Model):
    nombre = models.CharField("Nombre empresa", max_length=255, blank = False, null = False)

    def __str__(self):
        return self.nombre


class Departamento(models.Model):
    nombre = models.CharField("Nombre departamento", max_length=255, blank = False, null = False)

    def __str__(self):
        return self.nombre

class EmpresaDepartamentos(models.Model):
    fkempresa = models.ForeignKey(Empresa,on_delete=models.CASCADE,)
    fkdepartamento = models.ForeignKey(Departamento,on_delete=models.CASCADE,)

class Empleado(models.Model):
    nombre = models.CharField("Nombre", max_length=255, blank = False, null = False)
    apellido = models.CharField("Apellido", max_length=255, blank = False, null = False)
    fechaNacimiento = models.DateField("Fecha de nacimiento", blank = False, null = False)
    correo = models.CharField("Correo electrónico", max_length=255, blank = False, null = False)
    genero = models.CharField("Genéro", max_length=10, blank = True, null = True)
    telefono = models.CharField("Teléfono", max_length=15, blank = True, null = True)
    celular = models.CharField("Celular", max_length=15, blank = True, null = True)
    fechaIngreso = models.DateField("Fecha de Ingreso", blank = False, null = False)
    fkArea = models.ForeignKey(EmpresaDepartamentos,on_delete=models.CASCADE,)
    #fkdepartamento = models.ForeignKey(Departamento,on_delete=models.CASCADE,)

    def __str__(self):
        return self.nombre    