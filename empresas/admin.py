from django.contrib import admin

# Register your models here.
from .models import *
admin.site.register(Empresa)
admin.site.register(Departamento)
admin.site.register(EmpresaDepartamentos)
admin.site.register(Empleado)