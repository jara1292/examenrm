from empresas.forms import EmpleadoForm
from django.shortcuts import render, redirect, get_object_or_404
from .models import Empleado
#from .forms import EmpresaDepForm
from django.views.generic import ListView, DetailView
# Create your views here.

#Listar deps
def empleados(ListView):
    empleados =  Empleado.objects.all()
    context = {'empleados': empleados}
    return render(ListView, 'empleados.html', context)

    
# #crear dep
def crearempdep(request):
    if request.method == 'POST':
        form = EmpleadoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('empleados')
    form = EmpleadoForm()

    return render(request,'crear-empleado.html',{'form': form})

# #editar dep
def editaremp(request, pk, template_name='editar-empleados.html'):
    empleado = get_object_or_404(Empleado, pk=pk)
    form = EmpleadoForm(request.POST or None, instance=empleado)
    if form.is_valid():
        form.save()
        return redirect('empleados')
    return render(request, template_name, {'form':form})

# #eliminar dep
# def deletedep(request, pk, template_name='eliminardeps.html'):
#     departamento = get_object_or_404(Departamento, pk=pk)
#     if request.method=='POST':
#         departamento.delete()
#         return redirect('index')
#     return render(request, template_name, {'object':departamento})