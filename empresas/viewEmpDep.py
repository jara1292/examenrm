from django.shortcuts import render, redirect, get_object_or_404
from .models import EmpresaDepartamentos
from .forms import EmpresaDepForm
from django.views.generic import ListView, DetailView
# Create your views here.

#Listar deps
def ListarEmpresaDeps(ListView):
    empdep =  EmpresaDepartamentos.objects.all()
    context = {'empdep': empdep}
    return render(ListView, 'empresa-dep.html', context)

    
# #crear dep
def crearempdep(request):
    if request.method == 'POST':
        form = EmpresaDepForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('empresa-dep')
    form = EmpresaDepForm()

    return render(request,'asignar-emp-dep.html',{'form': form})

# #editar dep
# def editdep(request, pk, template_name='editardep.html'):
#     departamento = get_object_or_404(Departamento, pk=pk)
#     form = DepartamentosForm(request.POST or None, instance=departamento)
#     if form.is_valid():
#         form.save()
#         return redirect('listardeps')
#     return render(request, template_name, {'form':form})

# #eliminar dep
# def deletedep(request, pk, template_name='eliminardeps.html'):
#     departamento = get_object_or_404(Departamento, pk=pk)
#     if request.method=='POST':
#         departamento.delete()
#         return redirect('index')
#     return render(request, template_name, {'object':departamento})