from django.shortcuts import render, redirect, get_object_or_404
from .models import Departamento
from .forms import DepartamentosForm
from django.views.generic import ListView, DetailView
# Create your views here.

#Listar deps
def ListaDepartamentos(ListView):
    departamentos =  Departamento.objects.all()
    context = {'departamentos': departamentos}
    return render(ListView, 'listardeps.html', context)

    
#crear dep
def creardep(request):
    if request.method == 'POST':
        form = DepartamentosForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('listardeps')
    form = DepartamentosForm()

    return render(request,'creardeps.html',{'form': form})

#editar dep
def editdep(request, pk, template_name='editardep.html'):
    departamento = get_object_or_404(Departamento, pk=pk)
    form = DepartamentosForm(request.POST or None, instance=departamento)
    if form.is_valid():
        form.save()
        return redirect('listardeps')
    return render(request, template_name, {'form':form})

#eliminar dep
def deletedep(request, pk, template_name='eliminardeps.html'):
    departamento = get_object_or_404(Departamento, pk=pk)
    if request.method=='POST':
        departamento.delete()
        return redirect('index')
    return render(request, template_name, {'object':departamento})