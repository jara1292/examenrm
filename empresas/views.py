from django.shortcuts import render, redirect, get_object_or_404
from .models import Empresa
from .forms import EmpresasForm
from django.views.generic import ListView, DetailView
# Create your views here.

#Listar empresas
def InicioView(ListView):
    empresas =  Empresa.objects.all()
    context = {'empresas': empresas}
    return render(ListView, 'inicio.html', context)

    
#detalle de empresa
class EmpresaDetailView(DetailView):
    model = Empresa
    template_name = 'empresa-detalle.html'

#crear empresa
def create(request):
    if request.method == 'POST':
        form = EmpresasForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('inicio')
    form = EmpresasForm()

    return render(request,'crear.html',{'form': form})

#editar empresa
def edit(request, pk, template_name='editar.html'):
    empresa = get_object_or_404(Empresa, pk=pk)
    form = EmpresasForm(request.POST or None, instance=empresa)
    if form.is_valid():
        form.save()
        return redirect('index')
    return render(request, template_name, {'form':form})

#eliminar empresa
def delete(request, pk, template_name='confirmar_eliminar.html'):
    empresa = get_object_or_404(Empresa, pk=pk)
    if request.method=='POST':
        empresa.delete()
        return redirect('index')
    return render(request, template_name, {'object':empresa})