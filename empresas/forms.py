from django import forms
from .models import Departamento, Empleado, Empresa, EmpresaDepartamentos

class EmpresasForm(forms.ModelForm):
    class Meta:
        model = Empresa
        fields = "__all__"


class DepartamentosForm(forms.ModelForm):
    class Meta:
        model = Departamento
        fields = "__all__"

class EmpresaDepForm(forms.ModelForm):
    class Meta:
        model = EmpresaDepartamentos
        fields = "__all__"

class EmpleadoForm(forms.ModelForm):
    class Meta:
        model = Empleado
        fields = "__all__"