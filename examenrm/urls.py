"""examenrm URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from empresas import views,viewsDeps,viewEmpDep,viewsEmp
from django.conf.urls import include
urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('',views.InicioView, name = 'index'),
    path('empresas/inicio', views.InicioView, name='inicio'),
    path('empresas/<int:pk>/', views.EmpresaDetailView.as_view(), name='detalle'),
    path('empresas/editar/<int:pk>/', views.edit, name='editar'),
    path('empresas/crear/', views.create, name='crear'),
    path('empresas/eliminar/<int:pk>/', views.delete, name='eliminar'),
    path('empresas/listardeps', viewsDeps.ListaDepartamentos, name='listardeps'),
    path('empresas/editardeps/<int:pk>/', viewsDeps.editdep, name='editardep'),
    path('empresas/creardeps/', viewsDeps.creardep, name='creardeps'),
    path('empresas/eliminardeps/<int:pk>/', viewsDeps.deletedep, name='eliminardep'),
    path('empresas/emp-dep', viewEmpDep.ListarEmpresaDeps, name='empresa-dep'),
    path('empresas/asignar-emp-dep/', viewEmpDep.crearempdep, name='asignar-emp-dep'),
    path('empresas/empleados/', viewsEmp.empleados, name='empleados'),
    path('empresas/crearempleado/', viewsEmp.crearempdep, name='crear-empleado'),
    path('empresas/editaremp/<int:pk>', viewsEmp.editaremp, name='editaremp'),
]
